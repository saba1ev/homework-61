import React, { Component } from 'react';
import './App.css';
import RestCountry from "./Container/RestCountry/RestCountry";

class App extends Component {
  render() {
    return (
      <div className="App">
        <RestCountry/>
      </div>
    );
  }
}

export default App;
