import React from 'react';

const CountryInfo = (props) => {
  return (
    props.info ? <div className='InfoCountry'>
      <h3>Country info {props.info.name}</h3>
      <div className='Info'>
        <p><strong>Capital: </strong>{props.info.capital}</p>
        <p><strong>Population: </strong>{props.info.population}</p>
        <h4>Borders:</h4>
        <ul>
          {props.info.borders.map(border => <li key={border}>{border}</li>)}
        </ul>
      </div>
      <div className='Flag'>
        <img src={props.info.flag} alt="Flag"/>
      </div>
    </div> : null
  );
};

export default CountryInfo;