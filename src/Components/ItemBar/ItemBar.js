import React, {Component, Fragment} from 'react';


class ItemBar extends Component {

  render() {
    return (
      <Fragment>
        {this.props.rest.map((item, index)=>{
          return(
            <div onClick={()=> this.props.click(item.alpha3Code)} key={index}>
              <p>{item.name}</p>
            </div>
          )
        })}
        <div>
          <p></p>
        </div>
      </Fragment>
    );
  }
}

export default ItemBar;