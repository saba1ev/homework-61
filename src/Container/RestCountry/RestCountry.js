import React, {Component, Fragment} from 'react';
import './RestCountry.css';
import ItemBar from "../../Components/ItemBar/ItemBar";
import Axios from "axios";
import CountryInfo from "../../Components/CountryInfo/CountryInfo";



class RestCountry extends Component {
  state={
    country: [],
    oneCountry: null
  };
  getAlphaThreeCode = () =>{
    Axios.get('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code')
      .then(response =>{
        this.setState({country: response.data});
      })
  };

  getInfo = (alpha) =>{
    Axios.get(`https://restcountries.eu/rest/v2/alpha/${alpha}`)
      .then(response=>{
        Promise.all(response.data.borders.map(border => {
          return Axios.get(`https://restcountries.eu/rest/v2/alpha/${border}`)
        })).then(countries => {
          let borders = [];
          countries.forEach(country => {
            borders.push(country.data.name)
          });
          response.data.borders = borders;
          this.setState({
            oneCountry: response.data
          })
        })
      })
  };

  componentDidMount(){
    this.getAlphaThreeCode();
  }
  render() {
    return (
      <Fragment>
        <div className='Container'>
          <div className='ScrollBar'>
            <h5>Country</h5>
            <ItemBar
              click={this.getInfo}
              rest={this.state.country}
            />
          </div>
          <CountryInfo
            info={this.state.oneCountry}
          />
        </div>
      </Fragment>
    );
  }
}

export default RestCountry;